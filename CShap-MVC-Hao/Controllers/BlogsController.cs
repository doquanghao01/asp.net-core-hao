﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using CShap_MVC_Hao.Modles;
using CShap_MVC_Hao.Repository;
using Microsoft.AspNetCore.Mvc;

namespace CShap_MVC_Hao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogsController : ControllerBase
    {
        private BlogRepository blogRepository = new BlogRepository(new BlogContext());

        // GET: api/Blogs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Blog>>> GetBlogs()
        {
            var blogs = await blogRepository.Get();
            if (!blogs.Any())
            {
                return NotFound();
            }
            return blogs;
        }

        // GET: api/Blogs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Blog>> GetBlog(int id)
        {
            var blog = await blogRepository.GetDetail(id);
            if (blog == null)
            {
                return NotFound();
            }
            return blog;
        }

        // PUT: api/Blogs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBlog(int id, Blog blog)
        {
            
            if (await blogRepository.Put(id, blog) == false)
            {
                return NotFound();

            }
            return NoContent();
        }

        // POST: api/Blogs
        [HttpPost]
        public async Task<ActionResult<Blog>> PostBlog(Blog blog)
        {
            if (await blogRepository.Post(blog) == false)
            {
                return NotFound();
            }

            return CreatedAtAction("GetBlog", new { id = blog.Id }, blog);
        }

        // DELETE: api/Blogs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBlog(int id)
        {
            if (await blogRepository.Delete(id)==false)
            {
                return NotFound();
            }

            return NoContent();
        }


    }
}
