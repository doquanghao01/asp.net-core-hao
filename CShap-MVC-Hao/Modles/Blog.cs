﻿using System;
using System.Collections.Generic;

namespace CShap_MVC_Hao.Modles
{
    public partial class Blog
    {
        public int Id { get; set; }
        public string? Title { get; set; }
        public string Categorize { get; set; } = null!;
        public int? Status { get; set; }
        public string Location { get; set; } = null!;
        public DateTime? PublicDay { get; set; }
        public string? Picture { get; set; }
        public string? Detail { get; set; }
        public string? ShortDescription { get; set; }
    }
}
