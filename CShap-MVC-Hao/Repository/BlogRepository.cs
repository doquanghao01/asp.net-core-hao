﻿using Microsoft.EntityFrameworkCore;
using CShap_MVC_Hao.Modles;

namespace CShap_MVC_Hao.Repository
{
    public class BlogRepository
    {
        private readonly BlogContext _context;

        public BlogRepository(BlogContext context)
        {
            _context = context;
        }
        //Get data from database
        public async Task<List<Blog>> Get()
        {
            return await _context.Blogs.ToListAsync();
        }

        //Get details from database
        public async Task<Blog> GetDetail(int id)
        {
            return await _context.Blogs.FindAsync(id); ;
        }
        //Post to database
        public async Task<bool> Post(Blog blog)
        {
            _context.Blogs.Add(blog);
            await _context.SaveChangesAsync();
            if (GetDetail(blog.Id) == null)
            {
                return false;
            }
            return true;
        }
        //Delete to database
        public async Task<bool> Delete(int id)
        {
            if (GetDetail(id) == null)
            {
                return false;
            }
            try
            {
                _context.Blogs.Remove(await GetDetail(id));
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }
        //Put to database
        public async Task<bool> Put(int id, Blog blog)
        {
            if (id != blog.Id)
            {
                return false;
            }
            try
            {
                _context.Entry(blog).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BlogExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }
        private bool BlogExists(int id)
        {
            return (_context.Blogs?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }

}
